#!/usr/bin/env bash
function usage(){
cat << EOF
Syncronizes a list of files from levante's ICON pool to local ICON pool.
Usage: $0 <machine> <exp> <target>
where
    <machine>           name of the remote machine: an entry with the same name must be configure in ~/.ssh/config
                        Available machines: levante
    <file-exp>          List of files requires by an experiment (see e.g. rsc/exp/aes_bubble_test)
    <target>            Path of the local target directory (e.g. /pool/data/ICON)

Requires bash4 and rsync.
EOF
}

# Evaluate command line arguments
[[ $# -lt 1 ]] || [[ $1 == '-h' ]] && usage && exit 0
[[ $# -ne 3 ]]   && usage && exit 1
[[ $# -gt 3 ]]   && usage && exit 2

# Evaluate <file-machine> argument
case "$1" in
    levante)
        remote_ssh="levante"
        remote_pool_root="/pool/data/ICON"
        ;;
    *)
        echo "Error! Machine not implemented yet: $1" 2>&1
        exit 3
        ;;
esac

# Evaluate <file-exp> argument
[[ ! -f $2 ]] && echo "Invalid exp file." 2>&1 && exit 4
rsync_list="$2"

# Evaluate <target> argument
[[ ! -d $3 ]] && usage && exit 5
local_pool_root="$3"

# Run rsync
rsync -v --files-from=${rsync_list} ${remote_ssh}:${remote_pool_root}/ ${local_pool_root}/
